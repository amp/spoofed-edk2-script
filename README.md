# Build Dependencies
| Distro        | Dependencies |
| ------------- | ------------ |
|  Arch-based   | sudo pacman -S --needed git make nasm iasl |
|  Ubuntu-based | sudo apt install -y git make nasm iasl build-essential |
|  Fedora-based | sudo dnf install -y git make nasm iasl |

# Usage
```
curl -O https://codeberg.org/amp/spoofed-edk2-script/src/branch/main/spoofed-edk2-script.sh
chmod +x spoofed-edk2-script.sh
sudo ./spoofed-edk2-script.sh
```