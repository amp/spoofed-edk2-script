#!/bin/bash

edk2_git="https://github.com/tianocore/edk2.git"
edk2_branch="edk2-stable202402"
edk2_dir="$(pwd)/spoofed-edk2-script"

function main()
{
  edk2_compile
}

function edk2_compile()
{
  if [[ -e $edk2_dir ]]; then
    rm -rf $edk2_dir
  fi

  mkdir -p $edk2_dir
  cd $edk2_dir

  git clone --branch $edk2_branch $edk2_git $edk2_dir
  git submodule update --init

  bios_vendor="American Megatrends"
  bios_version="P2.70"
  bios_release_date="02/07/2023"
  bios_id="AMI"
  
  sed -i "s/\"EDK II\"/\"$bios_vendor\"/" $edk2_dir/MdeModulePkg/MdeModulePkg.dec
  sed -i "s/\"EDK II\"/\"$bios_vendor\"/" $edk2_dir/ShellPkg/ShellPkg.dec
  sed -i "s/\"EFI Development Kit II / OVMF\"/\"$bios_vendor\"/" $edk2_dir/OvmfPkg/Bhyve/SmbiosPlatformDxe/SmbiosPlatformDxe.c
  sed -i "s/\"0.0.0\"/\"$bios_version\"/" $edk2_dir/OvmfPkg/Bhyve/SmbiosPlatformDxe/SmbiosPlatformDxe.c
  sed -i "s/\"02/06/2015\"/\"$bios_release_date\"/" $edk2_dir/OvmfPkg/Bhyve/SmbiosPlatformDxe/SmbiosPlatformDxe.c
  sed -i "s/\"http://www.tianocore.org/edk2/\"/\"$bios_vendor\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c
  sed -i "s/\"edk2\"/\"$bios_id\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c
  sed -i "s/\"EmulatorPkg\"/\"$bios_id\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c
  sed -i "s/\"https://svn.code.sf.net/p/edk2/code/trunk/edk2/EmulatorPkg/\"/\"$bios_id\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c
  sed -i "s/\"https://svn.code.sf.net/p/edk2/code/trunk/edk2/EmulatorPkg/EmulatorPkg.dsc\"/\"$bios_id\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c
  sed -i "s/\"OSV\"/\"$bios_id\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c
  sed -i "s/\"malloc\"/\"$bios_id\"/" $edk2_dir/EmulatorPkg/PlatformSmbiosDxe/SmbiosTable.c

  make -j$(nproc) -C BaseTools
  . edksetup.sh
  OvmfPkg/build.sh -p OvmfPkg/OvmfPkgX64.dsc -a X64 -b RELEASE -t GCC5
}
main
